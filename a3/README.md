> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Jimmy Anderson

### Assignment #3 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. MySQL workbench installation
3. Developed database ERD and forward engineered locally
4. Chapter Questions (Ch 7- 8)

#### Deliverables:
1. Entity Relationship Diagram (ERD)
2. Working database with 10 records in each table
3. docs folder: a3.mwb and a3.sql
4. img folder: a3.png (ERD PIC)
5. Bitbucket and useful links

#### README.md file should include the following items:

* Screenshot of Entity Relationship Diagram (ERD)

#### Assignment Screenshot:
*Screenshot A3 ERD*:
![A3 ERD](img/a3.png "ERD based upon A3 Requirements")

#### Assignment Files:
*A3.MWB*:
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")  
*A3.SQL*:
[A3 SQL File](docs/a3.sql "A3 SQL Script")

#### Bitbucket Repository Links:

*LIS4368 Repository:*
[LIS4368](https://bitbucket.org/jda17d/lis4368/ "LIS4368 Repository")

*BitBucketStationLocations Repository:*
[BitBucketStationLocations](https://bitbucket.org/jda17d/bitbucketstationlocations/ "BitBucketStationLocations Repository")

#### Tutorial Links:

*Pet Database Tutorial:*
[Pet Database Tutorial](http://www.qcitr.com/vids/LIS4381_A3_Database_Requirements.mp4 "PetDatabaseTutorial")