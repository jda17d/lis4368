> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Jimmy Anderson

### Assignment #2 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Java Servlet Development
3. Chapter Questions (Ch 5- 6)

#### README.md file should include the following items:

* a. http://localhost:9999/hello
* b. http://localhost:9999/hello/HelloHome.html
* c. http://localhost:9999/hello/sayhello
* d. http://localhost:9999/hello/querybook.html
* e. http://localhost:9999/hello/sayhi


#### Assignment Screenshots:


*Screenshot of http://localhost:9999/hello wihtout index.html*:
![http://localhost:9999/hello without index.html](img/A2P1.png)


*Screenshot of http://localhost:9999/hello/index.html*:
![http://localhost:9999/hello/index.html with index.html](img/A2P2.png)


*Screenshot of http://localhost:9999/hello/sayhello servlet*:
![http://localhost:9999/hello/sayhello](img/A2P3.png)


*Screenshot of http://localhost:9999/hello/querybook.html servlet*:
![http://localhost:9999/hello/querybook.html](img/A2P4.png)

*Screenshot of http://localhost:9999/hello/sayhi servlet*:
![http://localhost:9999/hello/sayhi](img/A2P5.png)

*Screenshot of http://localhost:9999/hello/querybook.html servlet with selection*:
![http://localhost:9999/hello/querybook.html with selection](img/A2P6.png)

*Screenshot of query results from querybook.html*:
![Query Results](img/A2P7.png)

#### Bitbucket Repository Links:

*LIS4368 Repository:*
[LIS4368](https://bitbucket.org/jda17d/lis4368/ "LIS4368 Repository")

*BitBucketStationLocations Repository:*
[BitBucketStationLocations](https://bitbucket.org/jda17d/bitbucketstationlocations/ "BitBucketStationLocations Repository")


#### Tutorial Links:

*Tutorial: Java Servlets:*
[Java Web Servlet Tutorial](https://www.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html/ "Java Web Servlet Tutorial")