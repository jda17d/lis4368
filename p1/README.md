> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Jimmy Anderson

### Project #1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Jquery Validation
3. Regexp Data Control
4. Chapter Questions (Ch 9- 10)

#### Deliverables:
1. Screenshot of failed validation
2. Screenshot of passed validation
3. Chapter questions


#### README.md file should include the following items:

* Screenshot of failed form validation
* Screenshot of passed form validation

#### Assignment Screenshot:

*Screenshot failed form validation*:
![Failed Validation](img/failed.png "Failed form validation")

*Screenshot passed form validation*:
![Passed Validation](img/passed.png "Passed form validation")

#### Bitbucket Repository Links:

*LIS4368 Repository:*
[LIS4368](https://bitbucket.org/jda17d/lis4368/ "LIS4368 Repository")

*BitBucketStationLocations Repository:*
[BitBucketStationLocations](https://bitbucket.org/jda17d/bitbucketstationlocations/ "BitBucketStationLocations Repository")

#### Tutorial Links:

*Jquery/regexp project:*
[Jquery/regexp project](https://www.youtube.com/watch?v=PmVF__bBQtE "DataValidationTutorial")