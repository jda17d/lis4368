> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Jimmy Anderson

### Assignment #1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Ch 1- 4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above)
* Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial)
* got commands w/short descriptions:
* Bitbucket repo links: a) this assignment and b) the completed tutorial above /bitbucketstationlocations).
* http://localhost:9999/lis4368/

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push -  Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git diff - Show changes between commits, commit and working tree, etc

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Tomcat running http://localhost:9999*:

![Tomcat Installation Screenshot](img/tomcat.png)

*Local Host Web App:*
[http://localhost:9999/lis4368/](http://localhost:9999/lis4368/ "Local Host Web App")

#### Bitbucket Repository Links:

*LIS4368 Repository:*
[LIS4368](https://bitbucket.org/jda17d/lis4368/ "LIS4368 Repository")

*BitBucketStationLocations Repository:*
[BitBucketStationLocations](https://bitbucket.org/jda17d/bitbucketstationlocations/ "BitBucketStationLocations Repository")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[BitBucketStationLocation Tutorial](http://www.qcitr.com/usefullinks.htm#lesson3b "BitbucketStationLocation")

*Tutorial: Install Apache Tomcat:*
[Tomcat Installation Tutorial](https://www.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html "Tomcat Installation Tutorial")

*Tutorial: Install JDK:*
[JDK Installation Tutorial](https://www.ntu.edu.sg/home/ehchua/programming/howto/JDK_HowTo.html#Set-JAVA_HOME "JDK Installation Tutorial")