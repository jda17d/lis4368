> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Jimmy Anderson

### Assignment #4 Requirements:

*Sub-Heading:*

1. Design server-side validation for the customer form data
2. Compiled Customer and CustomerServelet
3. Tested data validation and XSS protection
4. Chapter Questions (Ch 11- 12)

#### Deliverables:
1. README.md with screenshots of failed & passed server-side validation
2. Chapter (11-12) assignment quiz

#### README.md file should include the following items:

* Screenshot of failed server-side validation
* Screenshot of passed server-side validation

#### Assignment Screenshot:

*Screenshot failed server-side validation*:
![failed server side validation](img/failed.PNG)

*Screenshot passed server-side validation*:
![passed server side validation](img/passed.PNG)

#### Bitbucket Repository Links:

*LIS4368 Repository:*
[LIS4368](https://bitbucket.org/jda17d/lis4368/ "LIS4368 Repository")

*BitBucketStationLocations Repository:*
[BitBucketStationLocations](https://bitbucket.org/jda17d/bitbucketstationlocations/ "BitBucketStationLocations Repository")

#### Tutorial Links:

*Assignment A4 overview:*
[Assignment A4 overview](https://youtu.be/9U7ylWlHefA "Assignment A4 overview")

*Assignment A4 overview P2:*
[Assignment A4 overview P2](https://youtu.be/OgruO27SzEg "Assignment A4 overview P2")

*Assignment A4 overview P3:*
[Assignment A4 overview P3](https://youtu.be/DXMXJiv1K0s "Assignment A4 overview P3")

*Assignment A4 overview P4:*
[Assignment A4 overview P4](https://youtu.be/yeJzyMZKTto "Assignment A4 overview P4")

*Assignment A4 overview P5:*
[Assignment A4 overview P5](https://youtu.be/LZOLszyJnfM "Assignment A4 overview P5")

*Assignment A4 overview P6:*
[Assignment A4 overview P6](https://youtu.be/A8VyeVZ7hM8 "Assignment A4 overview P6")

*Debugging:*
[Debugging](https://youtu.be/Y060J5_V4yg "Debugging")