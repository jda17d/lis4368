> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Jimmy Anderson

### Project 2 Requirements:

*Sub-Heading:*

1. Modified customers.jsp to display customers in table
2. Created/Edited modify.jsp to update customer info (with validation)
3. Modified CustomerSevlet to be compliant with new CRUD features (loads fields with edit)
4. Updated CustomerDB to allow the other CRUD features to interact with the database
    (Update, Delete, Select + Email exists)
5. Tested validation and new CRUD capabilities + verified changes within the database
6. Chapter Questions (Ch 16- 17)

#### Deliverables:
1. README.md with screenshots of valid entered data, passed validation, display data, modify form, modified data, delete warning, and associated database changes.
2. Chapter (16-17) assignment quiz

#### README.md file should include the following items:

* Screenshot of valid data entered
* Screenshot of passed validation (thanks.jsp)
* Screenshot of displayed data (customers.jsp)
* Screenshot of modify form (modify.jsp)
* Screenshot of modified data (customers.jsp)
* Screenshot of delete warning (customers.jsp)
* Screenshot of associated database changes (Select, Insert, Update, Delete)

#### Assignment Screenshot:

Valid User Form Entry (customerform.jsp) |  Passed Validation (thanks.jsp)
:----------------------------------:|:------------------------------------:
![](img/ValidEntry.PNG)  |  ![](img/PassedValidation.PNG)

Display Data (customers.jsp):
![Display Data](img/DisplayData.PNG)

Modify Form (modify.jsp) |  Modified Data (customers.jsp)
:----------------------------------:|:------------------------------------:
![](img/ModifyForm.PNG)  |  ![](img/ModifiedData.PNG)

Delete Warning (customers.jsp):
![Delete Warning](img/DeleteWarning.PNG)

Associated Database Entry (Select, Insert) |  Associated Database Entry (Update, Delete)
:----------------------------------:|:------------------------------------:
![](img/FirstHalfDB.PNG)  |  ![](img/SecondHalfDB.PNG)

#### Bitbucket Repository Links:

*LIS4368 Repository:*
[LIS4368](https://bitbucket.org/jda17d/lis4368/ "LIS4368 Repository")

*BitBucketStationLocations Repository:*
[BitBucketStationLocations](https://bitbucket.org/jda17d/bitbucketstationlocations/ "BitBucketStationLocations Repository")

#### Tutorial Links:

*Project P2 overview:*
[Project P2 overview](https://youtu.be/b0YNljkJfUE "Project P2 overview")

*Project P2 overview P2:*
[Project P2 overview P2](https://youtu.be/jGTgMBbOrfk "Project P2 overview P2")

*Project P2 overview P3:*
[Project P2 overview P3](https://youtu.be/PPjWJWGWya0 "Project P2 overview P3")

*Debugging:*
[Debugging](https://youtu.be/Y060J5_V4yg "Debugging")