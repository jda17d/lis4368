> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications

## Jimmy Anderson

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Created Bitbucket repo
    - Completed Bitbucket tutorials
    - Installed JDK
    - Installed Tomcat
    - Installed Ampps
    - Provided git commands
    - Provided screenshots of installations

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Configured Ampps
    - Developed Hello-world Java Servlet
    - Developed Database Java Servlet
    - Installed MySQL JDBC Driver
    - Provided screenshots of Servlet responses   

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Installed Mysql Workbench
    - Configured local forward engineering
    - Developed ERD of pet database
    - Provided .MWB file of database
    - Provided .SQL file for database
    - Provided screenshot of database ERD
    - Successfully forward engineered

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Created regexps to validate form data
    - Tested client side validation
    - Provided screenshots of various validation passes/fails
    - Redesigned slides images and text on home carousel

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Developed server-side validation for form data
    - Compiled Customer and CustomerServlet
    - Tested the server-side validation to ensure functionality 
    - Ensured XSS protection was working properly
    - Provided screenshots of passed/failed server-side validation

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Created a new crud package: data
    - Developed DBUtil.java to cleanup database resources
    - Developed ConnectionPool.java to reduce resource load on database
    - Developed CustomerDB.java to insert the data into the database
    - Altered CustomerServlet.java to allow data to pass into CustomerDB
    - Recompiled all java files and servlet
    - Verified that data is being transfered from the webapp to the database

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Modified customers.jsp to display customers in table
    - Created/Edited modify.jsp to update customer info (with validation)
    - Modified CustomerSevlet to be compliant with new CRUD features (loads fields with      edit)
    - Updated CustomerDB to allow the other CRUD features to interact with the database      (Update, Delete, Select + Email exists)
    - Tested validation and new CRUD capabilities
    - Verified changes within the database