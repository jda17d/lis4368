> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Jimmy Anderson

### Assignment #5 Requirements:

*Sub-Heading:*

1. Develop DBUtil to cleanup database resources
2. Develop ConnectionPool to reduce resource load on database
3. Developed CustomerDB.java to insert the data into the database
4. Altered CustomerServlet to allow data to pass into CustomerDB
5. Verified that data is being transfered from the webapp to the database
6. Chapter Questions (Ch 13- 15)

#### Deliverables:
1. README.md with screenshots of entered data, passed verification, and        database entries
2. Chapter (13-15) assignment quiz

#### README.md file should include the following items:

* Screenshot of data entered into customer form
* Screenshot of passed verification
* Screenshot of added database entry

#### Assignment Screenshot:

Valid User Form Entry (customerform.jsp) |  Passed Validation (thanks.jsp)
:----------------------------------:|:------------------------------------:
![](img/ValidForm.PNG)  |  ![](img/Passed.PNG)

*Associated Database Entry*:
![failed server side validation](img/DBentry.PNG)

#### Bitbucket Repository Links:

*LIS4368 Repository:*
[LIS4368](https://bitbucket.org/jda17d/lis4368/ "LIS4368 Repository")

*BitBucketStationLocations Repository:*
[BitBucketStationLocations](https://bitbucket.org/jda17d/bitbucketstationlocations/ "BitBucketStationLocations Repository")

#### Tutorial Links:

*Assignment A5 overview:*
[Assignment A5 overview](https://youtu.be/XPy0k1TGOPw "Assignment A5 overview")

*Assignment A5 overview P2:*
[Assignment A5 overview P2](https://youtu.be/MIXLqvY6Mms "Assignment A5 overview P2")

*Debugging:*
[Debugging](https://youtu.be/Y060J5_V4yg "Debugging")